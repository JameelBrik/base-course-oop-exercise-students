package AerialVehicles;

import Entities.Capability;
import Entities.Coordinates;
import java.util.*;

public class Eitan extends Haron {

//    AerialVehicleCapabilities capability;
//    Set<Capability> capabilitySet;

    Eitan(Coordinates cords){
        super(cords);
        capabilitySet = EnumSet.of(Capability.ATTACK, Capability.INTELLIGENCE);
    }

    public void executeAbility(){ capability.initiate(); }
    public void cancelAbility(){ capability.cancel(); }
    public void finishAbility(){ capability.finish(); }

}
