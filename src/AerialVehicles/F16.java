package AerialVehicles;

import Entities.Capability;
import Entities.Coordinates;
import java.util.*;

public class F16 extends FighterJets{

//    AerialVehicleCapabilities capability;
//    Set<Capability> capabilitySet;

    public F16(Coordinates cords){
        super(cords);
        capabilitySet = EnumSet.of(Capability.ATTACK, Capability.BDA);
    }

}
