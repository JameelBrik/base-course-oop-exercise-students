package AerialVehicles;

import Entities.Capability;
import Entities.Coordinates;
import java.util.*;

public class F15 extends FighterJets{

//    AerialVehicleCapabilities capability;
//    Set<Capability> capabilitySet;

    F15(Coordinates cords){
        super(cords);
        capabilitySet = EnumSet.of(Capability.ATTACK, Capability.INTELLIGENCE);
    }
}
