package AerialVehicles;

import Entities.Coordinates;

public class Haron extends UAVs{
    double max_flight_hours;
    Haron(Coordinates init_coordinates){
//        flight_hours = 0.0;
//        status = Status.READY;
//        base_coordinates = new Coordinates(init_coordinates.getLongitude(), init_coordinates.getLatitude());
        super(init_coordinates);
        max_flight_hours = 150;

    }

    @Override
    public void check() {
        if (flight_hours < max_flight_hours){
            status = Status.READY;
        }else{
            status = Status.NOTREADY;
            repair();
        }
    }
}
