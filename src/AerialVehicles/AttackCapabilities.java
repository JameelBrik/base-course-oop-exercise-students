package AerialVehicles;

import Entities.Missile;
import Entities.Capability;

public class AttackCapabilities extends AerialVehicleCapabilities{

    int missiles_num;
    Missile missile_type;

    public AttackCapabilities(int missilesNumber, Missile missileType, Capability name){
        super(name);
        missiles_num = missilesNumber;
        missile_type = missileType;
    }

    public void initiateAttack(){ System.out.println("Attack Initiated"); }

    public void cancelAttack(){ System.out.println("Attack Canceled"); }

    public void finishAttack(){ System.out.println("Attack Finished"); }

    public void fireMissiles(int num){ missiles_num -= num; }

    @Override
    public void initiate() {
        initiateAttack();
    }

    @Override
    public void cancel() {
        cancelAttack();
    }

    @Override
    public void finish() {
        finishAttack();
    }

    @Override
    public String returnCapability() {
        return missile_type.name() + "X" + missiles_num;
    }


}
