package AerialVehicles;

import Entities.Coordinates;

public abstract class UAVs extends AerialVehicle{

    UAVs(Coordinates init_coordinates){
        super(init_coordinates);
    }

    public void hoverOverLocation(Coordinates destination){
        status = Status.MIDAIR;
        System.out.println("Hovering Over: " + destination.getLongitude() + destination.getLatitude());
    }
}
