package AerialVehicles;
import Entities.Capability;
import Entities.Coordinates;

import java.util.Set;


public abstract class AerialVehicle {

    enum Status{
        READY, NOTREADY, MIDAIR;
    }
    double flight_hours;
    Status status;
    public Coordinates base_coordinates;
    public AerialVehicleCapabilities capability;
    Set<Capability> capabilitySet;

    public AerialVehicle(Coordinates init_coordinates){
        flight_hours = 0.0;
        status = Status.READY;
        base_coordinates = new Coordinates(init_coordinates.getLongitude(), init_coordinates.getLatitude());
    }

    public void flyTo(Coordinates destination){
        if (status == Status.READY){
            System.out.print("Flying to: " + destination.getLongitude() + base_coordinates.getLatitude());
            status = Status.MIDAIR;
        }else if (status == Status.NOTREADY){
            System.out.println("Aerial Vehicle isn't ready to fly");
        }
    }

    public void land(Coordinates destination){
        System.out.println("Landing on: " + destination.getLongitude() + destination.getLatitude());
        check();
    }

    public abstract void check();

    public void repair(){
        flight_hours = 0.0;
        status = Status.READY;
    }

    public boolean setCapability(AerialVehicleCapabilities input_capability){
        if (capabilitySet.contains(input_capability.capabilityName)){
            capability = input_capability;  //TODO: check if operator new should be used to create the capability
            return true;
        }else {
            System.out.println("Capability mismatch aerial vehicle");
            return false;
        }
    }

    public void executeAbility(){ capability.initiate(); }
    public void cancelAbility(){ capability.cancel(); }
    public void finishAbility(){ capability.finish(); }
}
