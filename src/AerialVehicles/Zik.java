package AerialVehicles;

import Entities.Capability;
import Entities.Coordinates;
import java.util.*;

public class Zik extends Hermes{
//    AerialVehicleCapabilities capability;
//    Set<Capability> capabilitySet;

    Zik(Coordinates cords){
        super(cords);
        capabilitySet = EnumSet.of(Capability.INTELLIGENCE, Capability.BDA);
    }
}