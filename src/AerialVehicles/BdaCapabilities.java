package AerialVehicles;

import Entities.Camera;
import Entities.Capability;

public class BdaCapabilities extends AerialVehicleCapabilities{

    Camera camera_type;

    public BdaCapabilities(Camera camera, Capability name){
       super(name);
        camera_type = camera;
    }

    public void initiateBda(){ System.out.println("Starting BDA"); }

    public void cancelBda(){ System.out.println("Canceled BDA"); }

    public void finishBda(){ System.out.println("Finished Bda"); }

    @Override
    public void initiate() {
        initiateBda();
    }

    @Override
    public void cancel() {
        cancelBda();
    }

    @Override
    public void finish() {
        finishBda();
    }

    @Override
    public String returnCapability() {
        return camera_type.name();
    }
}
