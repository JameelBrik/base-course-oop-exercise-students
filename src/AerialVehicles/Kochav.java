package AerialVehicles;

import Entities.Capability;
import Entities.Coordinates;
import java.util.*;

public class Kochav extends Hermes{
//    AerialVehicleCapabilities capability;
//    Set<Capability> capabilitySet;

    Kochav(Coordinates cords){
        super(cords);
        capabilitySet = EnumSet.of(Capability.ATTACK, Capability.INTELLIGENCE, Capability.BDA);
    }
}
