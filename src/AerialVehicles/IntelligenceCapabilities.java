package AerialVehicles;

import Entities.Sensor;
import Entities.Capability;

public class IntelligenceCapabilities extends AerialVehicleCapabilities {

    Sensor sensor_type;
    public IntelligenceCapabilities(Sensor sensor, Capability name){
        super(name);
        sensor_type = sensor;
    }

    public void collectIntel(){ System.out.println("Collecting information"); }

    public void cancelCollecting(){ System.out.println("Collecting information is canceled"); }

    public void finishCollecting(){ System.out.println("Finished collecting information");}

    @Override
    public void initiate() {
        collectIntel();
    }

    @Override
    public void cancel() {
        cancelCollecting();
    }

    @Override
    public void finish() {
        finishCollecting();
    }

    @Override
    public String returnCapability() {
        return sensor_type.name();
    }
}
