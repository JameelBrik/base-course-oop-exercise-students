package AerialVehicles;

import Entities.Capability;
public abstract class AerialVehicleCapabilities {

    Capability capabilityName;
    AerialVehicleCapabilities(Capability name){
        capabilityName = name;
    }

    public abstract void initiate();
    public abstract void cancel();
    public abstract void finish();
    public abstract String returnCapability();
}
