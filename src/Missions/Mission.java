package Missions;

import AerialVehicles.AerialVehicleCapabilities;
import Entities.Coordinates;
import AerialVehicles.AerialVehicle;
public abstract class Mission{
    Coordinates destination;
    String pilotName;
    AerialVehicle aerialVehicle;

    Mission(Coordinates cords, String name){
        destination = new Coordinates(cords.getLongitude(), cords.getLatitude());
        pilotName = name;
    }

    public void begin(){
        System.out.println("Beginning Mission!");
        aerialVehicle.flyTo(destination);
    }

    public void cancel(){
        System.out.println("Abort Mission!");
        aerialVehicle.land(aerialVehicle.base_coordinates);
    }

    public void finish(){
        /**
         * execute
         * */
        System.out.println(executeMission());
        aerialVehicle.land(aerialVehicle.base_coordinates);
        System.out.println("Finish Mission!");

    }

    public abstract String executeMission();

    public void assignVehicle(AerialVehicle vehicle){
        aerialVehicle = vehicle;
    }
    public void assignCapability(AerialVehicleCapabilities capabilities){
        aerialVehicle.setCapability(capabilities);
    }
}
