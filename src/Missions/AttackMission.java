package Missions;

import AerialVehicles.AerialVehicle;
import AerialVehicles.AerialVehicleCapabilities;
import AerialVehicles.AttackCapabilities;
import Entities.Capability;
import Entities.Coordinates;
import Entities.Missile;
import AerialVehicles.AttackCapabilities;

public class AttackMission extends Mission{
    String target;

    public AttackMission(Coordinates cords, String name, String input_target){
        super(cords, name);
        target = input_target;
    }

    @Override
    public String executeMission() {
        return pilotName + ": " + aerialVehicle.getClass().getSimpleName() + " Attacking suspect house with: " + aerialVehicle.capability.returnCapability();
    }

    public void setAttackCapability(int missilesNumber, Missile missileType, Capability name){
        AttackCapabilities weapons = new AttackCapabilities(missilesNumber, missileType, name);
        aerialVehicle.setCapability(weapons);
    }
}
