package Missions;

import Entities.Camera;
import Entities.Capability;
import Entities.Coordinates;
import AerialVehicles.BdaCapabilities;

public class BdaMission extends Mission{
    String objective;

    public BdaMission(Coordinates cords, String name, String objective){
        super(cords, name);
        this.objective = objective;
    }
    @Override
    public String executeMission() {
        return pilotName + ": " + aerialVehicle.getClass().getSimpleName() +
                "taking pictures of suspect " + objective + "with: " + aerialVehicle.capability.returnCapability() + "camera";
    }
    public void setBdaCapability(Camera camera, Capability name){
        BdaCapabilities weapons = new BdaCapabilities(camera, name);
        aerialVehicle.setCapability(weapons);
    }
}
