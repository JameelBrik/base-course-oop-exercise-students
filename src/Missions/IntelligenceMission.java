package Missions;

import Entities.Capability;
import Entities.Coordinates;
import AerialVehicles.IntelligenceCapabilities;
import Entities.Sensor;

public class IntelligenceMission extends Mission{
   String region;

   IntelligenceMission(Coordinates cords, String name, String region){
       super(cords, name);
       this.region = region;
   }
    @Override
    public String executeMission() {
        return pilotName + ": " + aerialVehicle.getClass().getSimpleName() + "collecting Data in" + region +
                "with sensor type: " + aerialVehicle.capability.returnCapability();
    }

    public void setIntelligenceCapability(Sensor sensor, Capability name){
       IntelligenceCapabilities weapons = new IntelligenceCapabilities(sensor, name);
       aerialVehicle.setCapability(weapons);
    }
}
