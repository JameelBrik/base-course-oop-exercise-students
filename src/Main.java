import AerialVehicles.*;
import Entities.*;
import Missions.*;

public class Main {

    public static void main(String[] args) {
	// Implement scenarios from readme here
        /***
         *      ATTACKING       *
         * **/
        Coordinates target_coords = new Coordinates(33.2033427805222, 44.5176910795946);
        Mission attackMission_mission = new AttackMission(target_coords, "Ze'ev Raz", "Tuwaitha Nuclear Research Center");
        Coordinates home_base = new Coordinates(31.827604665263365, 34.81739714569337);
        attackMission_mission.assignVehicle(new F16(home_base));
        attackMission_mission.assignCapability(new AttackCapabilities(250, Missile.SPICE250, Capability.ATTACK));
        attackMission_mission.begin();
        attackMission_mission.finish();

        /***
         *      BDA       *
         * **/
        Coordinates bda_target_coords = new Coordinates(33.2033427805222, 44.5176910795946);
        Mission bda_mission = new BdaMission(bda_target_coords, "Ilan Ramon", "Tuwaitha Nuclear Research Center");
        Coordinates bda_home_base = new Coordinates(31.827604665263365, 34.81739714569337);
        bda_mission.assignVehicle(new F16(bda_home_base));
        bda_mission.assignCapability(new BdaCapabilities(Camera.THERMAL, Capability.BDA));
        bda_mission.begin();
        bda_mission.finish();

        System.out.println("Good luck! ;)");
    }
}
