package Entities;

public enum Camera {
    REGULAR, THERMAL, NIGHTVISION;
}
